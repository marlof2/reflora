import { createRouter, createWebHistory } from "vue-router";

import RouteMaps from "../modules/maps/routes";
import Login from "../modules/login/routes";
import Inicio from "../modules/inicio/routes";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  scrollBehavior: function (to, from, savedPosition) {
    if (to.hash) {
      setTimeout(() => {
        const element = document.getElementById(to.hash.replace(/#/, ''))
        if (element && element.scrollIntoView) {
          element.scrollIntoView({ block: 'end', behavior: 'smooth' })
        }
      }, 500)
      return { el: to.hash };
    }
    else if (savedPosition) {
      return savedPosition
    }
    return { top: 0 }
  },
  routes: [
    ...RouteMaps,
    ...Login,
    ...Inicio,

  ],
});

export default router;
