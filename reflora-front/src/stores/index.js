import { createStore } from 'vuex'
import bootstrap from "bootstrap/dist/js/bootstrap.min.js";
import global from './global'


const store = createStore({
  state: {
    bootstrap,
  },
  modules: {
    global: global
  }
})

export default store