import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from './stores'
import materialKit from "./material-kit";
import Api from "./api";
// Nucleo Icons
import "./assets/css/nucleo-icons.css";
import "./assets/css/nucleo-svg.css";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


Api.init()
const app = createApp(App);

app.use(VueSweetalert2);
app.use(router);
app.use(materialKit);
app.use(store);
app.mount("#app");


